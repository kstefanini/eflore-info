django-cors-headers
Django>=2.0
djangorestframework>=3.7
django-jsonfield
invoke
jsonfield
progressbar2
gunicorn
