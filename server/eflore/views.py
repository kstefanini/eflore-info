from rest_framework import viewsets
from django.db.models import Q
from eflore.models import *
from eflore.serializers import SpeciesSerializer, TaxonSerializer

class SpeciesViewSet(viewsets.ModelViewSet):
    '''
    List of Species

    How to use queryset ? For example, I want to search 'rose' :
    GET /api/species/?search=rose
    will search by genus OR attribute_name
    and return a list of json

    '''
    queryset = Species.objects.all()
    serializer_class = SpeciesSerializer

    def get_queryset(self):
        qs = self.queryset
        search = self.request.query_params.get('search', None)
        if search:
            # need "from django.db.models import Q"
            # search with (genus__icontains=search or attribute_name__icontains)
            query = Q(genus__icontains=search)
            query.add(Q(attribute_name__icontains=search), Q.OR)
            query.add(Q(synonyms__icontains=search), Q.OR)
            query.add(Q(common_names__icontains=search), Q.OR)
            qs = qs.filter(query)
        return qs

class TaxonViewSet(viewsets.ModelViewSet):
    '''
    List of Taxons
    '''
    queryset = Taxon.objects.all()
    serializer_class = TaxonSerializer
