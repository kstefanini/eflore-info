from django.db import models
import jsonfield

# Create your models here.

class Taxon (models.Model) :
    parent = models.ForeignKey('Taxon', null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    nametype = models.CharField(max_length=100)

    def __str__(self):
        return "{} : {}".format(self.nametype, self.name)

class Species (models.Model) :
    parent = models.ForeignKey('Taxon', on_delete=models.CASCADE) # id-acer
    #genetic_genus = models.ForeignKey(Taxon, null=True)
    genus = models.CharField(max_length=100)
    attribute_name = models.CharField(max_length=100) # 'campestre'
    #scientific_name = models.CharField(max_length=100) # 'Acer campestre'
    synonyms = jsonfield.JSONField(null=True) # 'Acer Afine, Acer Wondracekii'
    common_names = jsonfield.JSONField(null=True) # 'Acéraille, Érable champêtre'
    ecology = jsonfield.JSONField(null=True)
    chorology = models.CharField(max_length=50)

    def __str__(self):
      return "{} {}".format(self.genus, self.attribute_name)

