from rest_framework import serializers
from eflore.models import Species, Taxon

class SpeciesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Species
        fields = "__all__"

class TaxonSerializer(serializers.ModelSerializer):
    parent = serializers.SerializerMethodField()
    class Meta:
        model = Taxon
        fields = ('nametype', 'name', 'parent')

    def get_parent(self, obj):
        if obj.parent is not None:
            return TaxonSerializer(obj.parent).data
        else:
            return None
