from django.apps import AppConfig


class EfloreConfig(AppConfig):
    name = 'eflore'
