#!/usr/bin/env python
# coding: utf-8
import os
import django
import sys
from invoke import task
import time
import progressbar
import json

def django_setup():
   sys.path.append(os.path.join(os.curdir, 'server'))
   os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings")
   django.setup()


@task
def build(ctx):
    '''
    (Re)Build server & client parts
    '''
    buildserver(ctx)
    buildclient(ctx)

@task
def buildserver(ctx):
    '''
    (Re)Build server parts
    '''
    ctx.run("pip install -r server/requirements.txt")

@task
def buildclient(ctx):
    '''
    (Re)Build client parts
    '''
    ctx.run("npm install")


@task
def migrate(ctx):
   '''
   Execute migrations
   '''
   django_setup()
   ctx.run("python server/manage.py migrate", pty=True)

@task
def makemigrations(ctx):
   '''
   Make migrations
   '''
   django_setup()
   ctx.run("python server/manage.py makemigrations", pty=True)

@task
def tables(ctx):
   '''
   Print database tables
   '''
   ctx.run("echo '.tables' | sqlite3 server/db.sqlite3", pty=True)

@task
def schema(ctx):
   '''
   Print database scheme
   '''
   ctx.run("echo '.schema' | sqlite3 server/db.sqlite3", pty=True)

@task
def runserver(ctx):
   '''
   Run server in dev mode
   '''
   django_setup()
   ctx.run("python server/manage.py runserver", pty=True)

@task
def runclient(ctx):
   '''
   Run client in dev mode
   '''
   ctx.run("cd client; npm run dev", pty=True)


@task
def createsuperuser(ctx):
   '''
   Create superuser
   '''
   django_setup()
   from django.contrib.auth import get_user_model
   User = get_user_model()
   superuser, created = User.objects.get_or_create(username='chris')
   superuser.set_password('eureka31')
   superuser.is_superuser = True
   superuser.is_staff = True
   superuser.save()


@task
def builddb(ctx):
   '''
   Create database from raw data
   '''
   updatedata(ctx)
   initdb(ctx)


@task
def updatedata(ctx):
    '''
    Update final_compiled_database.csv
    '''

    # delete database and recreate it empty
    ctx.run("rm -f server/data/compiled_database.json")

    def set_sc_names():
        # Open raw database downloaded on TelaBotanica (bdtfx) and MNHN (taxref)

        with \
        open('./server/data/raw_dbs/bdtfx.csv', 'r') as bdtfx,\
        open('./server/data/raw_dbs/taxref.txt', 'r') as taxref:

            bar1 = progressbar.ProgressBar(redirect_stdout=True)
            bar2 = progressbar.ProgressBar(redirect_stdout=True)

            name_dict = {}

            # Extract scientific name and scientifics synonyms from bdtfx database
            for idx, line in enumerate(bar1(bdtfx)):
                if idx == 0:
                    headers = line.split('\t')
                    index_status = headers.index('Statut du nom')
                    index_valid_name = headers.index('Nom retenu sans auteur')
                    index_synonym_name = headers.index('Nom sans auteur')
                else:
                    line = line.split('\t')
                    statut = line[index_status]
                    valid_name = line[index_valid_name]
                    synonym_name = line[index_synonym_name]

                    # Initialize valid scientific name if doesn't exist in name dictionnary
                    if valid_name not in name_dict:
                        name_dict[valid_name] = {'Synonymes scientifiques':[], 'Noms communs':[]}

                    # Append scientific synonyms to related valid scientific name dictionnary
                    if statut == 'synonyme' or statut == 'ambigu':
                        name_dict[valid_name]['Synonymes scientifiques'].append(synonym_name)

            # Extract common names from taxref database and append to related valid scientific name
            for idx, line in enumerate(bar2(taxref)):

                if idx == 0:
                    headers = line.split('\t')
                    index_valid_name = headers.index('NOM_COMPLET_HTML')
                    index_vern_name = headers.index('NOM_VERN')
                    index_regne = headers.index('REGNE')
                else:
                    line = line.split('\t')
                    regne = line[index_regne]
                    if regne == 'Plantae':
                        vern_name = [x.strip() for x in line[index_vern_name].split(',')]
                        valid_name = line[index_valid_name].replace('<i>','').split('</i>')[0]
                        if valid_name in name_dict:
                            for i in vern_name:
                                name_dict[valid_name]['Noms communs'].append(i)

            # Return dictionnary
            return name_dict

    def sanitize_baseflor():

        # Open raw databases downloaded on Philippe Julve Website (baseflor)

        bar1 = progressbar.ProgressBar(redirect_stdout=True)

        with \
        open('./server/data/raw_dbs/baseflor.csv', 'r') as baseflor, \
        open('./server/data/compiled_database.json', 'w') as export:

            baseflor_dict = {}

            name_dict = set_sc_names()

            ecology_list = ['Lumière','Température','Continentalité','Humidité atmosphérique','Humidité édaphique',\
                    'Réaction du sol (pH)','Niveau trophique','Salinité','Texture','Matière organique']

            taxonomy_list = ['Règne','Embranchement','Sub-embranchement','Classe','Subclasse','Clade intermédiaire 1',\
                    'Clade intermédiaire 2','Clade intermédiaire 3','Superordre','Clade intermédiaire 4',\
                    'Ordre','Famille','Subfamille']

            # Extract ecology, chorology and taxonomy data from the sheet

            for idx, line in enumerate(bar1(baseflor)):

                elements = line.split('\t')

                if idx >0 and elements[0] == '"esp"' and elements[-1] != '\n':

                    ecology = {}
                    index2 = 0
                    for index1, i in enumerate(elements):
                        i = i.replace('"','')
                        if index1 >= 25 and index1 <= 34 :
                            ecology[ecology_list[index2]] = i
                            index2 += 1

                    index3 = 0
                    taxonomy = {}
                    for index2, i in enumerate(elements):
                        i = i.replace('"','')
                        if index2 >= 42 and index2 <= 54 :
                            taxonomy[taxonomy_list[index3]] = i
                            index3 += 1

                    raw_sc_name = elements[59].replace('"','').replace('\n','')
                    genus = raw_sc_name.split(' ')[0]
                    attribute_name = raw_sc_name.split(' ')[1]
                    clean_sc_name = genus+' '+attribute_name
                    chorology = elements[5].replace('"','')

                    # And put it in a dictionnary

                    baseflor_dict[clean_sc_name] = {
                        'Genre':genus,
                        'Attribut':attribute_name,
                        'Chorologie':chorology,
                        'Ecologie':ecology,
                        'Taxonomie':taxonomy,
                        "Synonymes scientifiques":[],
                        "Noms communs":[]
                        }

            # Merge databases based on scientific name concordance

            for k1,v1 in name_dict.items():
                if k1 in baseflor_dict.keys():
                    baseflor_dict[k1]["Synonymes scientifiques"] = v1["Synonymes scientifiques"]
                    baseflor_dict[k1]["Noms communs"] = v1["Noms communs"]

            # Export data as a JSON file

            export.write(json.dumps(baseflor_dict))

    try:
        sanitize_baseflor()
    except:
         sys.exit('File non readable ! Check if sourcefile exist')


@task
def initdb(ctx):
    '''
    Erase sqlite database & populate it with data/*.csv
    '''
    django_setup()

    # delete database and recreate it empty
    ctx.run("rm -f server/db.sqlite3")
    ctx.run("rm -rf server/eflore/migrations")
    ctx.run("python server/manage.py makemigrations eflore")
    ctx.run("python server/manage.py migrate")
    createsuperuser(ctx)
    from eflore.models import Taxon, Species
    bar1 = progressbar.ProgressBar(redirect_stdout=True)
    bar2 = progressbar.ProgressBar(redirect_stdout=True)

    source = './server/data/compiled_database.json'

    def init_taxon_db(source):

        for k,attributes in bar1(source.items()):
            last_parent = None
            for taxon_name, name in attributes['Taxonomie'].items():
                if name != '':
                    last_parent,created = Taxon.objects.get_or_create(name=name, nametype=taxon_name, parent=last_parent)

        print('Taxons table populated succefully !')


    def init_species_db(source):

        for k,attributes in bar2(source.items()):
            subfamille = attributes['Taxonomie']['Subfamille']
            famille = attributes['Taxonomie']['Famille']

            if subfamille != '':
                for i in Taxon.objects.filter(name=subfamille):
                    parent_obj = i
            else :
                for i in Taxon.objects.filter(name=famille):
                    parent_obj = i

            Species.objects.get_or_create(parent=parent_obj, genus=attributes['Genre'],\
            attribute_name=attributes['Attribut'], chorology=attributes['Chorologie'],\
            ecology=json.dumps(attributes['Ecologie']), synonyms=json.dumps(attributes['Synonymes scientifiques']), common_names=json.dumps(attributes['Noms communs']))

        print('Species table populated succefully !')

    try:
    # try to open the sourcefile (type .csv)
        with open(source, 'r') as source_db :
            print("open file: {} and start populate taxon table".format(source))
            source_db = json.load(source_db)
            init_taxon_db(source_db)
            print("Start to populate species table".format(source))
            init_species_db(source_db)
    except:
        # if can't read correctly the file, catch an error
        sys.exit('File non readable ! Check if sourcefile exist')
